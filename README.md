# Assessment: Multiplication Table
Write an HTML page that uses Javascript to draw a 10x10 multiplication table. 
The key idea is to use two nested loops (one loop inside another) for the rows and columns of the table.